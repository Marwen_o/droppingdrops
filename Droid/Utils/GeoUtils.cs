﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Json;
using Android.Locations;

namespace DropingDrops.Droid
{
	public class GeoUtils
	{
		private static readonly DateTime Jan1st1970 = new DateTime
	   (1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		public static long CurrentTimeMillis()
		{
			return (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
		}

		public static JsonArray GetPoiInformation(Location userLocation, float mAzimuth, float inclination)
		{
			if (userLocation == null)
				return null;

			var pois = new List<JsonObject>();

			{
				var loc = POICoordinates(userLocation.Latitude, userLocation.Longitude, userLocation.Altitude, 20, mAzimuth, inclination);
				Console.WriteLine(userLocation.Latitude + "," + userLocation.Longitude + "," + userLocation.Altitude + "88888888" + loc[0] + "," + loc[1] + "," + loc[2]);
				var p = new Dictionary<string, JsonValue>(){
					{ "id","1" },
					{ "name", "POI#+1"  },
					{ "description", "This is the description of POI#"  },
					{ "latitude", loc[0] },
					{ "longitude", loc[1] },
					{ "altitude",loc[2] }
				};

				pois.Add(new JsonObject(p.ToList()));
			}

			var vals = from p in pois select (JsonValue)p;

			return new JsonArray(vals);
		}

		static double[] POICoordinates(double lat, double lon, double alt, double distance, float mazimuth, float incli)
		{
			double EarthRadius = 6371000;
			var inclin = distance * Math.Tan(incli - (Math.PI / 2));
			var azimuth = mazimuth * Math.PI / 180;

			var b = distance / EarthRadius;
			var a = Math.Acos(Math.Cos(b) * Math.Cos((90 - lat) * Math.PI / 180) + Math.Sin((90 - lat) * Math.PI / 180) * Math.Sin(b) * Math.Cos(azimuth));
			var c = Math.Asin(Math.Sin(b) * Math.Sin(azimuth) / Math.Sin(a));

			var POILat = 90 - (a * 180 / Math.PI);
			var POILong = (c * 180 / Math.PI) + lon;
			var POIAlt = alt + inclin;

			return new double[] { Math.Round(POILat, 5), Math.Round(POILong, 5), Math.Round(POIAlt, 5) };
		}
	}
}


